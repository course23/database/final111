/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.bookingkeepingapplication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class EntryDao {
    static{
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EntryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static final String userName="root", password="";
    
    public void addEntry(Entry entry) throws Exception{
        try(Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/bookkeeping", userName, password)){
         //新增一筆到 entries (15%)
        }
    }
    
    public void removeEntry(int id) throws Exception{
        try(Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/bookkeeping", userName, password)){
            //以 id 刪除一筆 entries (15%)
        }
    }
    
    public List<EntryReport> getReport() throws Exception{
        List<EntryReport> ret=new ArrayList<>();
        try(Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/bookkeeping", userName, password)){
            //從 entries 跟 account 資料表整合資料，需包含
            //entries.id, accountId, type, amount, bankAccount, accountName, description （30%）
            
        }
        return ret;
    }
    
    public double getAllExpense() throws Exception{
        try(Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/bookkeeping", userName, password)){
            //計算所有 type=0 的 entry 的 amount 總合(20%)
        }
        return -1;
    }
    
    public double getAllIncome() throws Exception{
        try(Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/bookkeeping", userName, password)){
            //計算所有 type=1 的 entry 的 amount 總合(20%)
        }
        return -1;
    }
}
