/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package rocks.imsofa.bookingkeepingapplication;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 *
 * @author USER
 */
public class BookingKeepingApplication {
    private EntryDao entryDao=new EntryDao();
    private BufferedReader reader=null;

    public BookingKeepingApplication() throws UnsupportedEncodingException {
        reader=new BufferedReader(new InputStreamReader(System.in, "utf-8"));
    }
    
    public static void main(String[] args) throws Exception{
        new BookingKeepingApplication().run();
    }
    
    private void run() throws Exception{
        while(true){
            System.out.println("1) Add new entry");
            System.out.println("2) Show Report");
            System.out.println("3) Sum All Expenses");
            System.out.println("4) Sum All Deposit");
            System.out.println("X) Quit");
            String command=reader.readLine();
            if("x".equals(command.toLowerCase())){
                break;
            }else if("1".equals(command)){
                this.addEntry();
            }else if("2".equals(command)){
                this.showReport();
            }else if("3".equals(command)){
                this.sumExpenses();
            }else if("4".equals(command)){
                this.sumDeposit();
            }
        }
    }
    
    private void addEntry() throws Exception{
        System.out.println("Account Id (1 or 2)");
        String accountId=reader.readLine();
        System.out.println("Type (0=expense  1=deposit)");
        String type=reader.readLine();
        System.out.println("Amount");
        String amount=reader.readLine();
        System.out.println("Description");
        String description=reader.readLine();
        Entry entry=new Entry();
        entry.setAccountId(Integer.valueOf(accountId));
        entry.setType(Integer.valueOf(type));
        entry.setAmount(Double.valueOf(amount));
        entry.setDescription(description);
        System.out.println("y/n?");
        String yn=reader.readLine();
        if("y".equals(yn)){
            entryDao.addEntry(entry);
        }
    }
    
    private void showReport() throws Exception{
        List<EntryReport> list=entryDao.getReport();
        for(EntryReport report : list){
            System.out.println(report.getBankAccount()+" "+report.getAccountName()+" "+report.getType()+" "+report.getAmount()+" "+report.getDescription());
        }
    }

    private void sumExpenses() throws Exception{
        System.out.println(entryDao.getAllExpense());
    }

    private void sumDeposit() throws Exception{
        System.out.println(entryDao.getAllIncome());
    }
}
